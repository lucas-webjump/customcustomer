/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery'
], function ($) {
    'use strict';

    function setDefaultHidden() {
    	// attr default
    	$('div[data-index="website_id"]').hide();
    	$('div[data-index="group_id"]').hide();
    	$('div[data-index="disable_auto_group_change"]').hide();
    	$('div[data-index="prefix"]').hide();
    	$('div[data-index="firstname"]').hide();
    	$('div[data-index="middlename"]').hide();
    	$('div[data-index="lastname"]').hide();
    	$('div[data-index="suffix"]').hide();
    	$('div[data-index="email"]').hide();
    	$('div[data-index="dob"]').hide();
    	$('div[data-index="taxvat"]').hide();
    	$('div[data-index="gender"]').hide();
    	$('div[data-index="sendemail_store_id"]').hide();
    	$('div[data-index="type_client"]').hide();

        // creating attr per setup
    	$('div[data-index="legal_type"]').hide();
    	$('div[data-index="nationality"]').hide();
    	$('div[data-index="company_name"]').hide();
    	$('div[data-index="trading_name"]').hide();
    	$('div[data-index="municipal_inscription"]').hide();
    	$('div[data-index="state_inscription"]').hide();
    }

    /**
     * [setShowPersonBrazilian show inputs with base select selected]
     * @param {[string]} person [type form]
     */
    function setShowPerson(person) {
    	$(person).each(function(item, value) {
    		var div = 'div[data-index="' + value + '"]';
    		$(div).show();
    	});
    }

    $(document).ready(function() {
    	var person_brazilian = ['website_id', 'firstname', 'lastname', 'cpf', 'street', 'number', 'zip_code', 'phone_contact', 'email', 'sendemail_store_id'];
    	var person_public_institution = ['website_id', 'firstname', 'lastname', 'social_name', 'cnpj', 'state_inscription', 'municipal_inscription', 'street', 'number', 'cep', 'phone_contact', 'email',  'sendemail_store_id'];
    	var person_juridical = ['website_id', 'firstname', 'lastname', 'social_name', 'cnpj', 'state_inscription', 'municipal_inscription', 'street', 'number', 'cep', 'phone_contact', 'email',  'sendemail_store_id'];

    	// set default hidden values
    	$(document).on('change', function() {
	    	setDefaultHidden();
            var value = $('select[name="customer[customer_type]"]').val();
            if (value === "1") {
                setShowPerson(person_brazilian);
            }
    	});

    	// function change select type person
	 	$(document).on('click', 'select[name="customer[customer_type]"]', function () {
     		var select = $('select[name="customer[customer_type]"]').val();
     		switch(select) {
     			case "1":
     				setShowPerson(person_brazilian);
     				break;
     			case '2':
     				setShowPerson(person_public_institution);
     				break;
 				case '3':
 					setShowPerson(person_juridical);
     				break;
     			default:
     				setDefaultHidden();
     		}
 		});
	});
});
